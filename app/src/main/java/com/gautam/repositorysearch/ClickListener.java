package com.gautam.repositorysearch;

import android.view.View;

/**
 * Created by gautam on 11/8/2017.
 */
public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}