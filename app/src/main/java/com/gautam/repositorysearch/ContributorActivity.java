package com.gautam.repositorysearch;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gautam.repositorysearch.Beans.Contributor;
import com.gautam.repositorysearch.Beans.Item;
import com.gautam.repositorysearch.adapter.ContributorAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gautam.repositorysearch.HomeActivity.REPO_ID;

public class ContributorActivity extends AppCompatActivity {
    public static final String PART_ID = "particular";
    public static final String URL_HTML = "URL";

    private static List<Contributor> contributorList;

    private GridView mGridContributors;
    private TextView tvName, tvProjectLink, tvDescription;

    ImageView ivAvatar;
    private ContributorAdapter mAdapter;
    private Item repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor);
        actionBarSetup();
        tvName = (TextView) findViewById(R.id.name);
        tvProjectLink = (TextView) findViewById(R.id.project_link);
        tvProjectLink.setMovementMethod(LinkMovementMethod.getInstance());
        tvDescription = (TextView) findViewById(R.id.description);
        ivAvatar = (ImageView) findViewById(R.id.avatar_img);
        mGridContributors = (GridView) findViewById(R.id.grid_contributors);
        repository = (Item) getIntent().getSerializableExtra(REPO_ID);
        DownloadPageTask downloadPageTask = new DownloadPageTask();
        downloadPageTask.execute("https://api.github.com/repos/" + repository.getOwner().getLogin() + "/" + repository.getName() + "/contributors");
        Glide.with(this).load(repository.getOwner().getAvatarUrl()).into(ivAvatar);
        tvName.setText(repository.getName());
        tvProjectLink.setText(repository.getHtmlUrl());
        tvDescription.setText(repository.getDescription());
        tvProjectLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent=new Intent(ContributorActivity.this,WebViewActivity.class);
                webIntent.putExtra(URL_HTML,repository.getHtmlUrl());
                startActivity(webIntent);
            }
        });
        mGridContributors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Contributor contributor = contributorList.get(i);
                Intent particularContributor=new Intent(ContributorActivity.this, ParticularContributorActivity.class);
                particularContributor.putExtra(PART_ID,contributor);
                startActivity(particularContributor);
            }
        });

    }


    private class DownloadPageTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... urls) {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urls[0]);

                urlConnection = (HttpURLConnection) url
                        .openConnection();


                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (listContributors(response.toString())) {


                    }
                }

                return response.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            mAdapter = new ContributorAdapter(ContributorActivity.this, contributorList);
            mGridContributors.setAdapter(mAdapter);

        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean listContributors(String in)
            throws JsonIOException, JsonSyntaxException,
            JsonParseException, IOException {
        contributorList = new ArrayList<>();

        Gson gson = new GsonBuilder().create();
        Contributor[] repository = gson.fromJson(in, Contributor[].class);
        contributorList = Arrays.asList(repository);

        return true;
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setTitle("");
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
