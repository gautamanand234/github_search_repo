package com.gautam.repositorysearch;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.gautam.repositorysearch.Beans.Item;
import com.gautam.repositorysearch.Beans.Repository;
import com.gautam.repositorysearch.adapter.RepositoryAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HomeActivity extends AppCompatActivity {


    private static List<Repository> repositoryList = new ArrayList<>();;
    private List<Item> repositoryLimitedTo10;
    private RecyclerView recyclerView;
    private RepositoryAdapter mAdapter;
    SearchView searchRepos;
    public static String REPO_ID = "repository";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        searchRepos = (SearchView) findViewById(R.id.search_repositories);
        actionBarSetup();
        searchRepos.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (internetConnectionCheck(HomeActivity.this)) {
                    DownloadPageTask downloadPageTask = new DownloadPageTask();
                    downloadPageTask.execute("https://api.github.com/search/repositories?q=" + query + "+language:assembly&sort=stars&order=desc");

                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (internetConnectionCheck(HomeActivity.this) && newText.length() > 2) {
                    DownloadPageTask downloadPageTask = new DownloadPageTask();
                    downloadPageTask.execute("https://api.github.com/search/repositories?q=" + newText + "+language:assembly&sort=stars&order=desc");

                }
                return false;
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Item repository = repositoryList.get(0).getItems().get(position);
                Intent newContributorAct = new Intent(HomeActivity.this, ContributorActivity.class);
                newContributorAct.putExtra(REPO_ID, repository);
                startActivity(newContributorAct);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean listRepositories(String in)
            throws JsonIOException, JsonSyntaxException,
            JsonParseException, IOException {
        repositoryList.clear();
        repositoryList = new ArrayList<>();
        Gson gson = new GsonBuilder().create();
        String res = "[" + in + "]";
        Repository[] repository = gson.fromJson(res, Repository[].class);
        repositoryList = Arrays.asList(repository);
        // Read file in stream mode
      /*  try (JsonReader reader = new JsonReader(in)) {
            //  reader.beginObject();

            reader.beginArray();
            while (reader.hasNext()) {
                // Read data into object model
                Repository repository = gson.fromJson(reader, Repository.class);
                System.out.println(repository);
                repositoryList.add(repository);
            }
        }*/
        return true;
    }

    private class DownloadPageTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... urls) {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urls[0]);

                urlConnection = (HttpURLConnection) url
                        .openConnection();


                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (listRepositories(response.toString())) {
                        Collections.sort(repositoryList, new Comparator<Repository>() {
                            public int compare(Repository o1, Repository o2) {
                                return o1.getItems().get(0).getWatchersCount() - o2.getItems().get(0).getWatchersCount();
                            }
                        });

                    }
                }
                return response.toString();
            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                settingAdapter("SearchedNow");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static boolean internetConnectionCheck(Activity CurrentActivity) {
        Boolean Connected = false;
        ConnectivityManager connectivity = (ConnectivityManager) CurrentActivity.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) for (int i = 0; i < info.length; i++)
                if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                    Log.e("My Network is: ", "Connected");
                    Connected = true;
                } else {
                }
        } else {
            Log.e("My Network is: ", "Not Connected");

            Toast.makeText(CurrentActivity.getApplicationContext(),
                    "Please Check Your internet connection",
                    Toast.LENGTH_LONG).show();
            Connected = false;

        }
        return Connected;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setTitle("Repository Search");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sort, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.size:
                settingAdapter("HighSize");
                return true;
            case R.id.open_issues:
                settingAdapter("LowIssues");
                return true;
            case R.id.score:
                settingAdapter("HighScore");
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void settingAdapter(String type) {

        if (type == "SearchedNow") {
            repositoryLimitedTo10 = new ArrayList<>();

            int repoListSize = 0;
            if (repositoryList.get(0).getItems().size() > 10) {
                repoListSize = 10;
            } else {
                repoListSize = repositoryList.get(0).getItems().size();
            }

            for (int i = 0; i <= repoListSize; i++) {
                repositoryLimitedTo10.add(repositoryList.get(0).getItems().get(i));
            }
            mAdapter = new RepositoryAdapter(repositoryLimitedTo10, HomeActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        } else if (type == "HighSize" && repositoryList.size()>0) {
            repositoryLimitedTo10 = new ArrayList<>();

            int repoListSize = 0;


            if (repositoryList.get(0).getItems().size() > 10) {
                repoListSize = 10;
            } else {
                repoListSize = repositoryList.get(0).getItems().size();
            }
            Collections.sort(repositoryList, new Comparator<Repository>() {
                public int compare(Repository o1, Repository o2) {
                    return (o1.getItems().get(0).getSize() - o2.getItems().get(0).getSize());
                }
            });
            for (int i = 0; i <= repoListSize; i++) {
                repositoryLimitedTo10.add(repositoryList.get(0).getItems().get(i));
            }


            mAdapter = new RepositoryAdapter(repositoryLimitedTo10, HomeActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        } else if (type == "LowIssues" && repositoryList.size()>0) {
            repositoryLimitedTo10 = new ArrayList<>();

            int repoListSize = 0;
            if (repositoryList.get(0).getItems().size() > 10) {
                repoListSize = 10;
            } else {
                repoListSize = repositoryList.get(0).getItems().size();
            }
            Collections.sort(repositoryList, new Comparator<Repository>() {
                public int compare(Repository o1, Repository o2) {
                    return o2.getItems().get(0).getOpenIssues() - o1.getItems().get(0).getOpenIssues();
                }
            });
            for (int i = 0; i <= repoListSize; i++) {
                repositoryLimitedTo10.add(repositoryList.get(0).getItems().get(i));
            }

            mAdapter = new RepositoryAdapter(repositoryLimitedTo10, HomeActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        } else if (type == "HighScore" && repositoryList.size()>0) {
            repositoryLimitedTo10 = new ArrayList<>();

            int repoListSize = 0;
            if (repositoryList.get(0).getItems().size() > 10) {
                repoListSize = 10;
            } else {
                repoListSize = repositoryList.get(0).getItems().size();
            }
            Collections.sort(repositoryList, new Comparator<Repository>() {
                public int compare(Repository o1, Repository o2) {
                    return (int) (o1.getItems().get(0).getScore() - o2.getItems().get(0).getScore());
                }
            });
            for (int i = 0; i <= repoListSize; i++) {
                repositoryLimitedTo10.add(repositoryList.get(0).getItems().get(i));
            }

            mAdapter = new RepositoryAdapter(repositoryLimitedTo10, HomeActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        }


    }
}
