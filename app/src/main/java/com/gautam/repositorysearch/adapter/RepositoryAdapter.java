package com.gautam.repositorysearch.adapter;

/**
 * Created by gautam on 11/8/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gautam.repositorysearch.Beans.Item;
import com.gautam.repositorysearch.R;

import java.util.List;


public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.MyViewHolder> {

    private List<Item> repositoryList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, fullName, watcherCount, starGazersCount;
        public ImageView avatarImage;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            fullName = (TextView) view.findViewById(R.id.full_name);
            watcherCount = (TextView) view.findViewById(R.id.watcher_count);
            starGazersCount = (TextView) view.findViewById(R.id.star_gazers);
            avatarImage = (ImageView) view.findViewById(R.id.iv_avatar);
        }
    }


    public RepositoryAdapter(List<Item> repositoryList, Context context) {
        this.repositoryList = repositoryList;
        mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item repository = repositoryList.get(position);
        holder.name.setText(String.valueOf(repository.getName()));
        holder.fullName.setText(repository.getFullName());
        holder.starGazersCount.setText(String.valueOf(repository.getStargazersCount()));
        holder.watcherCount.setText(String.valueOf(repository.getWatchersCount()));
        Glide.with(mContext).load(repository.getOwner().getAvatarUrl()).into(holder.avatarImage);
    }

    @Override
    public int getItemCount() {
        return repositoryList.size();
    }
}