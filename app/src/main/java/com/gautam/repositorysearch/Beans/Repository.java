
package com.gautam.repositorysearch.Beans;

import java.io.Serializable;
import java.util.List;

import com.gautam.repositorysearch.Beans.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Repository implements Serializable
{

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private Boolean incompleteResults;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    private final static long serialVersionUID = -289518212716497017L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Repository() {
    }

    /**
     *
     * @param items
     * @param totalCount
     * @param incompleteResults
     */
    public Repository(Integer totalCount, Boolean incompleteResults, List<Item> items) {
        super();
        this.totalCount = totalCount;
        this.incompleteResults = incompleteResults;
        this.items = items;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}