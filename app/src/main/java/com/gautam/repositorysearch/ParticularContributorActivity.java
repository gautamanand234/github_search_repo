package com.gautam.repositorysearch;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;
import com.gautam.repositorysearch.Beans.Contributor;
import com.gautam.repositorysearch.Beans.Item;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gautam.repositorysearch.ContributorActivity.PART_ID;
import static com.gautam.repositorysearch.HomeActivity.REPO_ID;

public class ParticularContributorActivity extends AppCompatActivity {
    private static List<Item> repoList;

    ListView listRepos;
    ImageView ivAvatar;
    Contributor particularContributor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_particular_contributor);
        listRepos = (ListView) findViewById(R.id.list_repos);
        ivAvatar = (ImageView) findViewById(R.id.avatar_img);
        particularContributor = (Contributor) getIntent().getSerializableExtra(PART_ID);
        actionBarSetup();
        DownloadPageTask downloadPageTask = new DownloadPageTask();
        downloadPageTask.execute(particularContributor.getReposUrl());
        Glide.with(this).load(particularContributor.getAvatarUrl()).into(ivAvatar);

        listRepos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Item repository = repoList.get(i);
                Intent newContributorAct = new Intent(ParticularContributorActivity.this, ContributorActivity.class);
                newContributorAct.putExtra(REPO_ID, repository);
                startActivity(newContributorAct);
            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean listRepositories(String in)
            throws JsonIOException, JsonSyntaxException,
            JsonParseException, IOException {
        repoList = new ArrayList<>();
        Gson gson = new GsonBuilder().create();

        Item[] repository = gson.fromJson(in, Item[].class);
        repoList = Arrays.asList(repository);

        return true;
    }

    private class DownloadPageTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... urls) {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urls[0]);

                urlConnection = (HttpURLConnection) url
                        .openConnection();


                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    listRepositories(response.toString());
                }
                return response.toString();
            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                ArrayList<String> repos = new ArrayList<>();
                for (Item item : repoList
                        ) {
                    repos.add((String) item.getName());
                }

                ArrayAdapter<String> arrayRepos = new ArrayAdapter<String>(ParticularContributorActivity.this, R.layout.row_repo, R.id.repo_item, repos);
                listRepos.setAdapter(arrayRepos);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setTitle("Contributor");
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
