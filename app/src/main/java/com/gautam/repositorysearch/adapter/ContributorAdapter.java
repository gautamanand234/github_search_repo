package com.gautam.repositorysearch.adapter;

/**
 * Created by gautam on 11/8/2017.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gautam.repositorysearch.Beans.Contributor;
import com.gautam.repositorysearch.R;

import java.util.ArrayList;
import java.util.List;

public class ContributorAdapter extends BaseAdapter {
    private Context mContext;
    List<Contributor> contributors;
    TextView contributorName;
    ImageView ivAvatar;

    // Constructor
    public ContributorAdapter(Context c, List<Contributor> contributorsList) {
        mContext = c;
        contributors =  contributorsList;
    }

    @Override
    public int getCount() {
        return contributors.size();
    }

    @Override
    public Object getItem(int position) {
        return contributors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contributor_list, parent, false);

        contributorName = (TextView) itemView.findViewById(R.id.contributor_name);
        ivAvatar = (ImageView) itemView.findViewById(R.id.iv_avatar);
        contributorName.setText(contributors.get(position).getLogin());
        Glide.with(mContext).load(contributors.get(position).getAvatarUrl()).into(ivAvatar);
        return itemView;
    }

}